package oksjonikeskkond;

import java.sql.ResultSet;
import java.sql.SQLException;

public class User {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String password;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public int getApartmentNumber() {
		return apartmentNumber;
	}

	public void setApartmentNumber(int apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

	private int id;
	private String firstName;
	private String lastName;
	private int phone;
	private int zip;
	private String country = "";
	private String municipality;
	private String street;
	private int houseNumber;
	private int apartmentNumber;

	public User(ResultSet resultSetRow) {
		try {
			this.email = resultSetRow.getString("email");
			this.password = resultSetRow.getString("password");
			this.id = resultSetRow.getInt("id");
			this.firstName = resultSetRow.getString("first_name");
			this.lastName = resultSetRow.getString("last_name");
			this.phone = resultSetRow.getInt("phone");
			this.zip = resultSetRow.getInt("zip");
			this.country = resultSetRow.getString("country");
			this.municipality = resultSetRow.getString("municipality");
			this.street = resultSetRow.getString("street");
			this.houseNumber = resultSetRow.getInt("house_number");
			this.apartmentNumber = resultSetRow.getInt("apartment_number");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public User() {

	}

}
