package oksjonikeskkond;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Product {

	private String productId;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(String productionYear) {
		this.productionYear = productionYear;
	}

	public int getStartingPrice() {
		return startingPrice;
	}

	public void setStartingPrice(int startingPrice) {
		this.startingPrice = startingPrice;
	}

	public String getAuctionStartTime() {
		return auctionStartTime;
	}

	public void setAuctionStartTime(String auctionStartTime) {
		this.auctionStartTime = auctionStartTime;
	}

	public String getAuctionCloseTime() {
		return auctionCloseTime;
	}

	public void setAuctionCloseTime(String auctionCloseTime) {
		this.auctionCloseTime = auctionCloseTime;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	private int userID;

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}
    
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String productName;
	private String productionYear;
	private int startingPrice;
	private String auctionStartTime;
	private String auctionCloseTime;
	private String artist;
	private String url;
	private int currentPrice;
	private String auctionStatus;

	public int getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(int currentPrice) {
		this.currentPrice = currentPrice;
	}

	public String getAuctionStatus() {
		return auctionStatus;
	}

	public void setAuctionStatus(String auctionStatus) {
		this.auctionStatus = auctionStatus;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Product() {
	}

	public Product(ResultSet resultSetRow) {
		try {
			this.id = resultSetRow.getInt("id");
			this.userID = resultSetRow.getInt("user_id");
			this.productName = resultSetRow.getString("product_name");
			this.productionYear = resultSetRow.getString("production_year");
			this.startingPrice = resultSetRow.getInt("starting_price");
			this.currentPrice = resultSetRow.getInt("current_price");
			this.auctionStartTime = resultSetRow.getString("auction_start_time");
			this.auctionCloseTime = resultSetRow.getString("auction_close_time");
			this.artist = resultSetRow.getString("artist");
			this.url = resultSetRow.getString("url");
			this.auctionStatus = resultSetRow.getString("auction_status");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
