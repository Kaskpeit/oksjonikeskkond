package oksjonikeskkond;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.company.Company_Service;
import ee.bcs.valiit.company.dao.Company;
import ee.bcs.valiit.rest.Auction;
import ee.bcs.valiit.rest.Register;

public class ProductService {

	public static Product getProduct(String productId) {

		ResultSet result = SqlHelper.performSql("select *,"
				+ " coalesce ((select max(price)"
				+ " from offer"
				+ " where offer.product_id = product.id),"
				+ " product.starting_price)"
				+ " as current_price, "
				+ " (IF(auction_close_time > now(), 'ACTIVE', 'CLOSED'))"
				+ " as auction_status"
				+ " from product"
				+ " where product.id=" + productId);
		try {
			while (result.next()) {
				return new Product(result);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void saveProduct(Product product) {
	String sql = "INSERT INTO product (userID, productId, productName, productionYear, startingPrice, auctionStartTime, auctionCloseTime, currentPrice, artist, url)";
		sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
				product.getUserID(), product.getProductId(), product.getProductName(), product.getProductionYear(),
				product.getStartingPrice(), product.getAuctionStartTime(), product.getAuctionCloseTime(),
				product.getCurrentPrice(), product.getArtist(), product.getUrl());
		SqlHelper.performSql(sql);
	}

	public static Offer saveOffer(Offer offer) {
		String sql = "INSERT INTO offer (product_id, price, user_id )";
		sql = sql
				+ String.format(" values ('%s','%s','%s')", offer.getProductId(), offer.getPrice(), offer.getUserId());
		SqlHelper.performSql(sql);
		return offer;

	}

	public static Offer[] getOffers(String productId) {
		List<Offer> offers = new ArrayList<>();
		ResultSet result = SqlHelper.performSql("select offer.*, user.first_name from offer inner join user on offer.user_id=user.id where offer.product_id=" + productId);
		try {
			while (result.next()) {
				offers.add(new Offer(result));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return offers.toArray(new Offer[0]);

	}

	public static Product[] getProducts() {
		List<Product> products = new ArrayList<>();
		ResultSet result = SqlHelper.performSql ("select *,"
						+ " coalesce ((select max(price)"
						+ " from offer"
						+ " where offer.product_id = product.id),"
						+ " product.starting_price)"
						+ " as current_price, "
						+ " (IF(auction_close_time > now(), 'ACTIVE', 'CLOSED')) "
						+ " as auction_status "
						+ " from product"
						+ " order by auction_status asc");
						
		try {
			while (result.next()) {
				products.add(new Product(result));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return products.toArray(new Product[0]);

	}

	public static Auction saveAuction(Auction auction) {
		String sql = "INSERT INTO product (user_id, product_name, artist, production_year, starting_price, auction_start_time, auction_close_time, url)";
		sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s','%s')", auction.getUserId(),
				auction.getProductName(), auction.getArtist(), auction.getProductionYear(), auction.getStartingPrice(),
				auction.getAuctionStartTime(), auction.getAuctionCloseTime(), auction.getUrl());
		SqlHelper.performSql(sql);
		return auction;
	}

	public static User registerUser(User user) {
		String sql = "INSERT INTO user (first_name, last_name, phone, zip, country, municipality, email, password, street, house_number, apartment_number)";
		sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')", user.getFirstName(), user.getLastName(), user.getPhone(), user.getZip(), user.getCountry(), user.getMunicipality(),
				user.getEmail(), user.getPassword(), user.getStreet(), user.getHouseNumber(),
				user.getApartmentNumber());
		SqlHelper.performSql(sql);
		return user;

	}
	
	public static User getProductOwner(String productId) {
		String sql = "select user.* from user inner join product on product.user_id = user.id where product.id = " + productId;
		ResultSet result = SqlHelper.performSql(sql);
		try {
			result.next();
			return new User(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean isAuctionClosed(String productId) {
		String sql = "select count(*) as auction_status from product where auction_close_time < now() and id = " + productId;
		ResultSet result = SqlHelper.performSql(sql);
		try {
			result.next();
			return result.getString(1).equals("1");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static User getAuctionWinner(String productId) {
		String sql = "select * from user where id = (select user_id from offer where product_id = " + productId + " order by price desc limit 1)";
		ResultSet result = SqlHelper.performSql(sql);
		try {
			result.next();
			return new User(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return new User();
	}

}