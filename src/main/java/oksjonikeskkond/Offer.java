package oksjonikeskkond;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Offer {



	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	private int productId;
	private int price;
	private int userId;
	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Offer() {
	}

	public Offer(ResultSet resultSetRow) {
		try {
			this.productId = resultSetRow.getInt("product_id");
			this.price = resultSetRow.getInt("price");
			this.userId = resultSetRow.getInt("user_id");
			this.firstName = resultSetRow.getString("first_name");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
