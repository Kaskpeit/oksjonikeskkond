package ee.bcs.valiit.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.company.dao.Company;

public class Company_Service {

	private final static String CONNECTION_URL = "jdbc:mysql://localhost:3306/companies";
	private final static String CONNECTION_USERNAME = "root";
	private final static String CONNECTION_PASSWORD = "tere";
	
	public static Company[] getCompanies() {
		List<Company> companies = new ArrayList<>();
		ResultSet result = Company_Service.performSqlSelect("select * from company");
		try {
			while (result.next()) {
				companies.add(new Company(result));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return companies.toArray(new Company[0]);
	}

	public static void saveCompany(Company company) {
		String sql = "INSERT INTO company (name, date_of_est, trading_name, management_b, supervisory_b, logo_url, industry, employee_count)";
		sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s','%s','%s')", company.getName(),
				company.getDate_of_est(), company.getTrading_name(), company.getManagement_b(),
				company.getSupervisory_b(), company.getLogo_url(), company.getIndustry(),
				company.getEmployee_count());
		performSqlSelect(sql);
	}

	public static ResultSet performSqlSelect(String sql) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection(CONNECTION_URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
			stmt = conn.createStatement();
			return stmt.executeQuery(sql);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

		
	}
}
