package ee.bcs.valiit.company.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Company {

	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate_of_est() {
		return date_of_est;
	}

	public void setDate_of_est(String date_of_est) {
		this.date_of_est = date_of_est;
	}

	public String getTrading_name() {
		return trading_name;
	}

	public void setTrading_name(String trading_name) {
		this.trading_name = trading_name;
	}

	public String getManagement_b() {
		return management_b;
	}

	public void setManagement_b(String management_b) {
		this.management_b = management_b;
	}

	public String getSupervisory_b() {
		return supervisory_b;
	}

	public void setSupervisory_b(String supervisory_b) {
		this.supervisory_b = supervisory_b;
	}

	public String getLogo_url() {
		return logo_url;
	}

	public void setLogo_url(String logo_url) {
		this.logo_url = logo_url;
	}

	public String getEmployee_count() {
		return employee_count;
	}

	public void setEmployee_count(String employee_count) {
		this.employee_count = employee_count;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	private String name;
	private String date_of_est;
	private String trading_name;
	private String management_b;
	private String supervisory_b;
	private String logo_url;
	private String employee_count;
	private String industry;
	
	public Company() {
	}

	public Company(ResultSet resultSetRow) {
		try {
		this.id = resultSetRow.getInt(1);
		this.name = resultSetRow.getString(2);
		this.date_of_est = resultSetRow.getString(3);
		this.trading_name = resultSetRow.getString(4);
		this.management_b = resultSetRow.getString(5);
		this.supervisory_b = resultSetRow.getString(6);
		this.logo_url = resultSetRow.getString(7);
		this.employee_count = resultSetRow.getString(8);
		this.industry = resultSetRow.getString(9);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
