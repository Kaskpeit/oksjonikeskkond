package ee.bcs.valiit.rest;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Auction {

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}

	public int getStartingPrice() {
		return startingPrice;
	}

	public void setStartingPrice(int startingPrice) {
		this.startingPrice = startingPrice;
	}
	
	public String getAuctionStartTime() {
		return auctionStartTime;
	}

	public void setAuctionStartTime(String auctionStartTime) {
		this.auctionStartTime = auctionStartTime;
	}

	public String getAuctionCloseTime() {
		return auctionCloseTime;
	}

	public void setAuctionCloseTime(String auctionCloseTime) {
		this.auctionCloseTime = auctionCloseTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	

	public int getUserId() {
		return userId;
	}

	
	public void setUserId(int userId) {
		this.userId = userId;
	}

	private int userId;
	private String productName;
	private String artist;
	private int productionYear;
	private int startingPrice;
	private String auctionStartTime;
	private String auctionCloseTime;
	private String url;

	public Auction() {

	}
	
	public Auction(ResultSet resultSetRow) {
		try {
			this.userId = resultSetRow.getInt("user_id");
			this.artist = resultSetRow.getString("artist");
			this.productionYear = resultSetRow.getInt("production_year");
			this.startingPrice = resultSetRow.getInt("starting_price");
			this.auctionStartTime = resultSetRow.getString("auction_start_time");
			this.auctionCloseTime = resultSetRow.getString("auction_close_time");
			this.url = resultSetRow.getString("url");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
