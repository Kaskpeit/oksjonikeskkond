package ee.bcs.valiit.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import ee.bcs.valiit.company.Company_Service;
import ee.bcs.valiit.company.dao.Company;
import ee.bcs.valiit.model.MessageDTO;
import oksjonikeskkond.Offer;
import oksjonikeskkond.Product;
import oksjonikeskkond.ProductService;
import oksjonikeskkond.User;
import oksjonikeskkond.UserService;

@Path("/")
public class RestResource {

	@GET
	@Path("/displayimage")
	@Produces("image/jpg")

	

	public Response getFullImage(@QueryParam("image_name") String imageName) throws IOException {

		BufferedImage image = null;
		image = ImageIO.read(new File("/Users/evelnsuving/oksjonpics/" + imageName));

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);

		byte[] imageData = baos.toByteArray();
		
		return Response.ok(imageData).build();
		
	}

	@POST
	@Path("/upload")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public String upload(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception {
		String UPLOAD_PATH = "/Users/evelnsuving/oksjonpicss/";
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
			// OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + "test.jpg"));
			while ((read = fileInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new WebApplicationException("Error while uploading file. Please try again !!");
		}
		return "OK";
	}

	@POST
	@Path("/registeruser")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String registerUser(User user) {
		ProductService.registerUser(user);
		return "OK";
	}

	@GET
	@Path("/signOut")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSignOut(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		session.setAttribute("user", null);
		return "OK";
	}

	@GET
	@Path("/getUser")
	@Produces(MediaType.APPLICATION_JSON)
	public User getCurrentUser(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		User currentUser = (User) session.getAttribute("user");
		if (currentUser != null) {
			return currentUser;
		} else {
			return new User();

		}
	}
	
	@GET
	@Path("/getWinner")
	@Produces(MediaType.APPLICATION_JSON)
	public User getWinner(@Context HttpServletRequest req, @QueryParam("product_id") String productId) {
		HttpSession session = req.getSession(true);
		User currentUser = (User) session.getAttribute("user");
		if (currentUser != null) {
			if (ProductService.isAuctionClosed(productId)) {
				User productOwner = ProductService.getProductOwner(productId);
				if (productOwner != null) {
					if (currentUser.getId() == productOwner.getId()) {
						return ProductService.getAuctionWinner(productId);
					}
				}
			}
		}
		
		return new User();
	}
	


	@POST
	@Path("/authenticate")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSignin(@Context HttpServletRequest req, @FormParam("email") String email,
			@FormParam("password") String password) {
		User currentUser = UserService.getUser(email, password);
		if (currentUser == null) {
			return "NOK";
		} else {
			HttpSession session = req.getSession(true);
			session.setAttribute("user", currentUser);
			return "OK";
		}

	}

	@POST
	@Path("saveAuction")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String saveAuction(@Context HttpServletRequest req, @FormDataParam("product_name") String productName, @FormDataParam("artist") String artist,
			@FormDataParam("production_year") String productionYear, @FormDataParam("starting_price") String startingPrice,@FormDataParam("auction_start_time") String auctionStartTime,
			@FormDataParam("auction_close_time") String auctionCloseTime, @FormDataParam("picture") InputStream picture, @FormDataParam("picture") FormDataContentDisposition pictureData) {
		HttpSession session = req.getSession(true);
		User currentUser = (User) session.getAttribute("user");
		
		if (currentUser != null) {
			
			String fileName = UUID.randomUUID().toString() + ".jpg";
			
			String UPLOAD_PATH = "/Users/evelnsuving/oksjonpics/";
			try {
				int read = 0;
				byte[] bytes = new byte[1024];
				
				OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileName));
				// OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + "test.jpg"));
				while ((read = picture.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				out.flush();
				out.close();
			} catch (IOException e) {
				throw new WebApplicationException("Error while uploading file. Please try again !!");
			}
			
			
			Auction auction = new Auction();
			auction.setUserId(currentUser.getId());
			auction.setProductName(productName);
			auction.setArtist(artist);
			auction.setProductionYear(Integer.parseInt(productionYear));
			auction.setStartingPrice(Integer.parseInt(startingPrice));
			auction.setAuctionStartTime(auctionStartTime);
			auction.setAuctionCloseTime(auctionCloseTime);
			auction.setUrl(fileName);
									
			ProductService.saveAuction(auction);		
			return "OK";
		} else {
			return "NOK";
		}
		
	}

	@GET
	@Path("/products")
	@Produces(MediaType.APPLICATION_JSON)
	public Product[] getProducts(@Context HttpServletRequest req) {
		return ProductService.getProducts();
	}

	@GET
	@Path("/offers")
	@Produces(MediaType.APPLICATION_JSON)
	public Offer[] getOffers(@QueryParam("product_id") String productId) {
		return ProductService.getOffers(productId);
	}

	@POST
	@Path("/saveOffer")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String saveOffer(@Context HttpServletRequest req, Offer offer) {
		HttpSession session = req.getSession(true);
		User currentUser = (User) session.getAttribute("user");
		if (currentUser != null) {
			offer.setUserId(currentUser.getId());
			ProductService.saveOffer(offer);
			return "OK";
		} else {
			return "NOK";
		}
	}

	@GET
	@Path("/product")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProduct(@QueryParam("product_id") String productId) {
		return ProductService.getProduct(productId);
	}

	

	@GET
	@Path("/companies")
	@Produces(MediaType.APPLICATION_JSON)
	public Company[] getCompanies() {
		return Company_Service.getCompanies();
	}

	@POST
	@Path("/company")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String saveCompany(Company company) {
		Company_Service.saveCompany(company);
		return "OK";
	}

	@GET
	@Path("/hi/text")
	@Produces(MediaType.TEXT_PLAIN)
	public String getHi(@DefaultValue("World") @QueryParam("name") String name) {
		return String.format("Hello, %s!", name);
	}

	@GET
	@Path("/hi/json/map/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHi2(@PathParam(value = "name") String name) {
		Map<String, String> response = new HashMap<>();
		response.put("greeting", String.format("Hello, %s!", name));
		return Response.ok(response).build();
	}

	@GET
	@Path("/hi/json/dto")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageDTO getHi3(@DefaultValue("World") @QueryParam("name") String name) {
		MessageDTO message = new MessageDTO();
		message.setText("Hello, " + name + "!!!");
		return message;
	}

	@POST
	@Path("/message")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String message(MessageDTO message) {
		return "Sõnum oli järgmine: " + message.getText();
	}

	@POST
	@Path("/addnumbers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNumbers(@FormParam("number1") String number1, @FormParam("number2") String number2) {
		int value1 = Integer.parseInt(number1);
		int value2 = Integer.parseInt(number2);
		int result = value1 + value2;
		Map<String, String> response = new HashMap<>();
		response.put("result", String.valueOf(result));
		response.put("comment", "See summa arvutati kokku RestResource klassis serveri pool.");
		return Response.ok(response).build();
	}

	@GET
	@Path("/message/db")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTextFromDb() throws SQLException, ClassNotFoundException {
		// Class.forName("com.mysql.cj.jdbc.Driver");
		Class.forName("org.mariadb.jdbc.Driver");
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/valiit", "root", "tere")) {
			try (Statement stmt = conn.createStatement()) {
				try (ResultSet rs = stmt.executeQuery("SELECT simple_column FROM simpledemo")) {
					rs.first();
					String result = rs.getString(1);
					conn.close();
					return result;

				}
			}
		}
	}
}
