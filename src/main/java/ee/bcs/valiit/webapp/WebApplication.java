package ee.bcs.valiit.webapp;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.freemarker.FreemarkerMvcFeature;

import ee.bcs.valiit.web.WebResource;

public class WebApplication extends ResourceConfig {
	public WebApplication() {
		super(WebResource.class);
		register(LoggingFeature.class);
		register(FreemarkerMvcFeature.class);
	}
}
