-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: oksjonikeskkond_product
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `oksjonikeskkond_product`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `oksjonikeskkond_product` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `oksjonikeskkond_product`;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `painting` varchar(45) DEFAULT NULL,
  `sculpture` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_table`
--

DROP TABLE IF EXISTS `new_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `new_table` (
  `product_id` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `user_id` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_table`
--

LOCK TABLES `new_table` WRITE;
/*!40000 ALTER TABLE `new_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `new_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offer`
--

DROP TABLE IF EXISTS `offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offer` (
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`price`,`user_id`),
  KEY `fk_offer_user_idx` (`user_id`),
  CONSTRAINT `fk_offer_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_offer_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offer`
--

LOCK TABLES `offer` WRITE;
/*!40000 ALTER TABLE `offer` DISABLE KEYS */;
INSERT INTO `offer` VALUES (1,35,1),(32,500,1),(62,67,1),(64,599,1),(1,555,2),(32,54,2),(34,54,2),(64,601,2),(33,1300,3),(33,1500,3),(62,290,3),(63,2200,3),(64,599,3),(33,1350,4),(35,2100,4),(62,299,6),(62,301,7),(63,2400,7),(63,2355,9),(63,2350,11);
/*!40000 ALTER TABLE `offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `production_year` int(11) DEFAULT NULL,
  `starting_price` int(11) NOT NULL,
  `auction_start_time` datetime NOT NULL,
  `auction_close_time` datetime NOT NULL,
  `artist` varchar(200) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_user_idx` (`user_id`),
  CONSTRAINT `fk_product_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,1,'\"Cypruses\"',2000,23,'2000-11-12 00:00:00','2002-12-11 00:00:00','Vincent Van Gogh','cypruses.jpg'),(32,2,'\"Starry Nights\"',1999,500,'2018-02-23 00:00:00','2018-03-23 00:00:00','Vincent Van Gogh','starrynights.jpeg'),(33,3,'\"Irises\"',1700,1200,'2018-03-13 00:00:00','2018-04-13 00:00:00','VIncent Van Gogh','irises.jpeg'),(34,1,'\"Sunflowers\"',2000,23,'2000-11-12 00:00:00','2002-12-11 00:00:00','Vincent Van Gogh','sunflowers.jpg'),(35,1,'\"Poppies\"',2000,550,'2000-11-12 00:00:00','2002-12-11 00:00:00','Vincent Van Gogh','poppies.jpg'),(62,1,'Kiss',1800,9000,'2018-03-03 00:00:00','2018-04-04 00:00:00','Klimt','86dc2919-046a-44f1-8372-fece0c114b26.jpg'),(63,3,'uus ',1919,2000,'2018-03-03 00:00:00','2018-04-04 00:00:00','maal','3ebf4759-b5de-4107-aa9b-6fbdb20d7917.jpg'),(64,1,'The Kiss',1888,600,'2018-03-03 00:00:00','2018-04-04 00:00:00','Klimt','3d8b5c18-5b33-4428-ac2b-0d971af83211.jpg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `municipality` varchar(200) DEFAULT NULL,
  `street` varchar(200) DEFAULT NULL,
  `house_number` int(11) DEFAULT NULL,
  `apartment_number` int(11) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Katrin','Kask','Katrin@mail.ee',511111111,999,'Estonia','Viljandi','Lai',1,131,'parool1'),(2,'Evelin','S','Evelin@mail.ee',522222222,111,'Estonia','Pärnu','Pikk',4,76,'parool2'),(3,'Mari','Maasikas','Mari@mail.ee',533333333,333,'Latvia','Riga','Valdemara',777,9,'parool3'),(4,'Karl','Kaalikas','Karl@mail.ee',544444444,222,'Finland','Helsingi','Pojonkatu',333,444,'parool4'),(6,'Maiu','Meie','Maiu@mail.ee',599999999,110000,'Estonia','Narva','Viru',111,0,'parool6'),(7,'Priit','Paul','Priit@mail.ee',58888888,666,'Estonia','Narva-Jõesuu','Pikk',98,0,'parool7'),(9,'Vitali','Nael','Vitali@mail.ee',57777777,789,'Russia','Moskva','Eesti',458,0,'parool9'),(11,'Iris','Tallinn','Iris@mail.ee',5444444,82,'Estonia','Saare','Lai',1,1,'parool11');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `companies`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `companies` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `companies`;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date_of_est` date DEFAULT NULL,
  `trading_name` varchar(450) DEFAULT NULL,
  `management_b` varchar(500) DEFAULT NULL,
  `supervisory_b` varchar(500) DEFAULT NULL,
  `logo_url` varchar(500) DEFAULT NULL,
  `employee_count` int(11) DEFAULT NULL,
  `industry` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'ArcoVara','1994-06-14','ARC1T','Tarmo Sild','Hillar-Peeter Luitsalu','http://www.nasdaqbaltic.com/upload/logos/arc.jpg',132,'Real Estate'),(3,'Nordecon','1996-04-11','NCN1T','Gerd Müller','Toomas Luman','http://www.nasdaqbaltic.com/upload/logos/ncn.gif',345,'Engineering'),(4,'LHV Group','2005-01-21','LHV1T','Madis Toomsalu','Rain Lõhmus','http://www.nasdaqbaltic.com/upload/logos/lhv.png',345,'Banking'),(14,'Tallinna Vesi','1999-12-12','TLV1T','null','null','http://www.nasdaqbaltic.com/upload/logos/tve.gif',298,'Utilities'),(17,'New ','2018-03-23','','null','null','',0,'');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-02  9:32:08
