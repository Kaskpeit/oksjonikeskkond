package ee.bcs.valiit.company;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.valiit.company.dao.Company;

public class CompanyServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPerformSqlSelect() throws SQLException {
		ResultSet result = Company_Service.performSqlSelect("select * from company");
		assertTrue(result.next());
		Company company1 = new Company(result);
		assertTrue(company1.getId() > 0);
	}
	
	@Test 
	public void testGetCompanies() {
		Company[] companies = Company_Service.getCompanies(); //tagastab company objektidena
		assertTrue(companies != null); //massiivi pole olemas
		assertTrue(companies.length > 0); //massiiv on tühi. ei ole samaväärne eelmisega, !=null-ga
		assertTrue(companies[0].getId() > 0);
	}
}
