package oksjonikeskkond;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProductServiceTest {

	private Offer offer;
	private int productId;



	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetProducts() {
		Product[] products = ProductService.getProducts();
		assertTrue(products != null);
		assertTrue(products.length > 0);
	}
	
	public void testSaveProduct() throws SQLException {
		Product product = new Product();
		product.setProductName("Test toote nimi");
		
		ProductService.saveProduct(product);
		
		ResultSet result = SqlHelper.performSql("SELECT MAX(id) FROM product");
		assertTrue(result.next());
		int newProductId = result.getInt(1);
		assertTrue(newProductId > 0);
		
		Product loadedProduct = ProductService.getProduct(String.valueOf(newProductId));
		assertTrue(loadedProduct != null);
		assertTrue(loadedProduct.getProductName().equals(product.getProductName()));

		Offer offers = new Offer();
	    offers.setProductId(loadedProduct.getId());
	    ProductService.saveOffer(offer);
	    
	    ResultSet result1 = SqlHelper.performSql("SELECT MAX(price) FROM offer");
		

//		assertTrue(offers != null);
//		assertTrue(newOffer > 0);
			 
		 
	}
	
	
	
	public void testgetOffer() {
		
	}
}
